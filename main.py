import os
import time
from datetime import datetime
import random
import sys
import json

DICTIONARY = "dictionary.json"
SCOREBOARD = "scoreboard.json"

HANGED_MAN = [
    """
    O
   /I\\
   / \\
""",
    """
|
|
|
|
|
|
""",
    """
__________
|/
|
|
|
|
|
""",
    """
_________
|/      |
|
|
|
|
|
""",
    """
_________
|/      |
|       O
|
|
|
|
""",
    """
_________
|/      |
|       O
|       I
|
|
|
""",
    """
_________
|/      |
|       O
|      /I
|
|
|
""",
    """
_________
|/      |
|       O
|      /I\\
|
|
|
""",
    """
_________
|/      |
|       O
|      /I\\
|        \\
|
|
""",
    """
_________
|/      |
|       O
|      /I\\
|      / \\
|
|
""",
]


def guess_letter(word, letter):
    list_word = list(word)

    if letter in list_word:
        return True

    else:
        return False


def replace_guess(current_guess, word, letter):
    list_guess = list(current_guess)
    list_word = list(word)
    starting_index = 0

    for i in range(list_word.count(letter)):
        index = list_word.index(letter, starting_index)
        list_guess[index] = letter
        starting_index = index + 1

    guess = "".join(list_guess)
    return guess


def death(word):
    print(HANGED_MAN[-1])
    print("Game over! The word was: %s" % word)

    while True:
        answer = input("Play again? (y/n) ").lower()

        if answer == "y" or answer == "yes":
            main(sys.argv)

        elif answer == "n" or answer == "no":
            quit()

        else:
            print("Please say yer or no.")
        

def get_timestamp():
    ts = time.time()
    string = datetime.fromtimestamp(ts).strftime("%Y-%m-%d_%H:%M:%S")
    return string


def clear_scoreboard():
    with open(SCOREBOARD, "w") as sb:
        clear_data = {}
        json.dump(clear_data, sb, indent=4)
        sb.close()


def set_scoreboard(word, guess, misses, result, difficulty):
    with open(SCOREBOARD, "r") as f:
        data = json.load(f)
        f.close()
    with open(SCOREBOARD, "w") as s:
        data[get_timestamp()] = {
            "Difficulty" : difficulty,
            "Result" : result,
            "Word" : word,
            "Final guess" : guess,
            "Mistakes" : misses
        }
                    
        json.dump(data, s, indent=4)
        s.close()


def save_game(word, current_guess, difficulty, guessed_letters, missed_letters, debug_mode):
    filename = get_timestamp() + "$" + current_guess + ".json"
    with open("savedatas/" + filename, "w") as f:
        data = {"word":word,
                "current_guess":current_guess,
                "difficulty":difficulty,
                "guessed_letters":guessed_letters,
                "missed_letters":missed_letters,
                "debug_mode":debug_mode}
        json.dump(data, f, indent=4)
        f.close()
    print(filename + " saved correctly")
    quit()
        
        
def load_data(filename):
    with open(filename, "r") as f:
        data = json.load(f)
        f.close()
    play(data["word"], data["debug_mode"], data["difficulty"], data["current_guess"], data["guessed_letters"], data["missed_letters"], filename)
    

def load_screen():
    print("Saved games. Format: timestamp$current_guess")
    savedatas = os.listdir("savedatas/")
    for f in savedatas:
        print(f)

    print("\nType the name of the savedata you would like to play, or type 'delete savedata' to delete that file.")
    while True:
        i = input()
        if i in savedatas:
            print("Loaded " + i)
            load_data("savedatas/" + i)

        elif i.split(" ")[0] == "delete" and i.split(" ")[1] in savedatas:
            os.remove("savedatas/" + i.split(" ")[1])
            print("Removed " + i)

        else:
            print("Please type a valid input.")
    
def show_scoreboard():
    print("SCOREBOARD\n")
    print("TIMESTAMP, DIFFICULTY, RESULT, WORD, FINAL GUESS, MISTAKES")
    with open(SCOREBOARD, "r") as sc:
        data = json.load(sc)
        sc.close()
    for i in data:
        print(i, data[i]["Difficulty"], data[i]["Result"], data[i]["Word"], data[i]["Final guess"], data[i]["Mistakes"])

        
def play(word, debug_mode, difficulty, current_guess="", guessed_letters=[], missed_letters=[], savefile=""):
    if current_guess == "":
        current_guess = "_" * len(word)

    if difficulty == "easy":
        hanged_man = HANGED_MAN

    elif difficulty == "medium":
        hanged_man = HANGED_MAN[:1]+HANGED_MAN[2:6]+HANGED_MAN[7:8]+HANGED_MAN[9:]

    elif difficulty == "hard":
        hanged_man = HANGED_MAN[:1]+HANGED_MAN[3:4]+HANGED_MAN[5:6]+HANGED_MAN[7:8]+HANGED_MAN[9:] 

    print(HANGED_MAN[len(missed_letters)])
    print("Welcome to the Hangman. Try to guess the word below by guessing letters. Each mistake will take this stickman closer to death. You can write 'quit' to exit the game or 'restart' to restart the game. You can also type 'save' to save this game and quit. You can load it later.")
    print(current_guess + " (Number of letters: %s)\n" % str(len(word)))

    if len(missed_letters) != 0:
        print("Mistakes: " + ", ".join(missed_letters))
    

    while current_guess != word:
        letter = input("Guess a letter: ").upper()

        if letter in guessed_letters or letter in missed_letters:
            print("You already said that letter.")

        elif letter == "?" and debug_mode:
            print(word)

        elif letter == "SAVE":
            save_game(word, current_guess, difficulty, guessed_letters, missed_letters, debug_mode)

        elif letter == "QUIT" or letter == "EXIT":
            quit()

        elif letter == "RESTART":
            main(sys.argv)

        elif not letter.isalnum():
            print("Please use an alphanumeric character.")

        elif len(letter) > 1:
            print("Only one letter at the time!")

        else:
            data = guess_letter(word, letter)

            if data:
                guessed_letters.append(letter)
                current_guess = replace_guess(current_guess, word, letter)
            else:
                missed_letters.append(letter)
                
                if (difficulty == "easy" and len(missed_letters) == len(HANGED_MAN) - 1) or (difficulty == "medium" and len(missed_letters) == 6) or (difficulty == "hard" and len(missed_letters) == 4):

                    set_scoreboard(word, current_guess, missed_letters, "Failure", difficulty)
                    print(HANGED_MAN[-1])
                    print("Game over! The word was: %s" % word)

                    while True:
                        answer = input("Play again? (y/n) ").lower()

                        if answer == "y" or answer == "yes":
                            main(sys.argv)

                        elif answer == "n" or answer == "no":
                            quit()

                        else:
                            print("Please say yer or no.")
                    
            print(hanged_man[len(missed_letters)])
            print(current_guess + " (Number of letters: %s)\n" % str(len(word)))

            if len(missed_letters) != 0:
                print("Mistakes: " + ", ".join(missed_letters))

    set_scoreboard(word, current_guess, missed_letters, "Success", difficulty)
    print("\nCongratulations, you won!")

    while True:
        answer = input("Play again? (y/n) ").lower()
        if answer == "y" or answer == "yes":
            main(sys.argv)
        elif answer == "n" or answer == "no":
            quit()
        else:
            print("Please say yer or no.")


def main(argv):
    if len(argv) > 1 and (argv[1] == "--debug" or argv[1] == "-d"):
        debug = True
    else:
        debug = False
    print(HANGED_MAN[-1]+"\n")
    print("The Hangman. Please type a difficulty, 'easy', 'medium' or 'hard', or access the load screen by typing 'load', or write 'scoreboard' to see the previous games. You can also clear the scoreboard by typing 'clear scoreboard', and exit by typing 'quit'.")
    while True:
        i = input("Please write an input: ").lower()

        if os.path.exists(DICTIONARY) and (i == "easy" or i == "medium" or i == "hard"):
            with open(DICTIONARY, "r") as f:
                data = json.load(f)
                word = data[i][random.randint(0, len(data[i]) - 1)]
                f.close()
                play(word, debug, i)

        elif os.path.exists(SCOREBOARD) and (i == "scoreboard"):
            show_scoreboard()

        elif os.path.exists(SCOREBOARD) and (i == "clear scoreboard"):
            clear_scoreboard()
            print("Scoreboard cleared.")

        elif os.path.exists("savedatas/") and (i == "load"):
           load_screen() 
            
        elif i == "quit" or i == "exit":
            print("Good bye.")
            quit()
        
        else:
            print("Please write a correct input.")


if __name__ == "__main__":
    main(sys.argv)
